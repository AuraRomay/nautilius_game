﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class ItemsData
{
    public string name;
    public int id;
    public int amountOfItems;
    public Sprite image;
    //Sell value
    public int value;
    //Shop value
    public int cost;



}
