﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct InvernaderosItemsData 
{
    public ItemsData seed;
    public ItemsData fruit;
}
