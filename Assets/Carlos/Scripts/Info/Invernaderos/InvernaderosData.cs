﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InvernaderosData
{

    public InvernaderosItemsData pack;
    public Transform position;
    public float time;

    private float saveTime;
    public bool timeStart;

    public void Init()
    {       
        saveTime = time;
    }

    public bool TimerEnd(float currentTime)
    {
        if (!timeStart)
            return false;
        time -= currentTime;
        if(time<=0)
        {
            time = saveTime;
            timeStart = false;
            return true;
        }
        return false;
    }

}
