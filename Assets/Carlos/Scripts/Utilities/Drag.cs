﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Drag : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public bool sell;
    public bool isIn;
    public bool dragging;
  

    [SerializeField] Vector3 originalPosition;
  
    void Start()
    {
        originalPosition = GetComponent<RectTransform>().position;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        isIn = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isIn = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(sell)
        {
            if (isIn)
            {
                if (Input.GetMouseButton(0))
                {
                    dragging = true;
                }
                else
                {
                    dragging = false;
                    MoveToOriginalPosition();
                }

            }
            if (dragging)
                transform.position = Input.mousePosition;

            if (Input.GetMouseButtonUp(0))
            {
                dragging = false;
                MoveToOriginalPosition();
            }
        }
       
           
    }

    void MoveToOriginalPosition()
    {
       GetComponent<RectTransform>().localPosition = new Vector3(0,0,0);
    }
}
