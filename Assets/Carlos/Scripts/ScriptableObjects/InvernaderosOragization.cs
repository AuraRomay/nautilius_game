﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="InvernaderosData")]
public class InvernaderosOragization : ScriptableObject
{
    public List<InvernaderosData> invernaderosActivos;
    public List<InvernaderosItemsData> elementosAceptables;

    public bool CheckSeed(ItemsData data,out int elementIndex)
    {
        for (int i = 0; i < elementosAceptables.Count; i++)
        {
            if(data.id==elementosAceptables[i].seed.id)
            {
                elementIndex = i;
                return true;
            }
        }
        elementIndex = -1;
        return false;
    }

    public InvernaderosItemsData GetPack(int elementIndex)
    {
        return elementosAceptables[elementIndex];
    }

    public void ResetItems(int elemetIndex)
    {
        elementosAceptables[elemetIndex].fruit.amountOfItems = 0;
        elementosAceptables[elemetIndex].seed.amountOfItems = 0;
    }
}
