﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName ="Items")]
public class ElementsOfItems : ScriptableObject
{

    public List<ItemsData> items;

    public void ResetAmount()
    {
        for (int i = 0; i < items.Count; i++)
        {
            items[i].amountOfItems = 0;
        }
    }
   
}
