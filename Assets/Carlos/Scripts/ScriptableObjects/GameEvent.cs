﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
[CreateAssetMenu(menuName = "Game Event")]
public class GameEvent : ScriptableObject
{
    public UnityEvent myEvent = new UnityEvent();
    public bool eventAdded;
    public void Raise()
    {
        myEvent.Invoke();
    }

    public void AddEvent(UnityAction action)
    {

        myEvent.AddListener(action);
        //UnityEditor.Events.UnityEventTools.AddPersistentListener(myEvent, action);
        eventAdded = true;

    }
}
