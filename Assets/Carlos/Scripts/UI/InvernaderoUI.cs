﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvernaderoUI : MonoBehaviour
{
    [SerializeField] UnityEngine.UI.Image seedImage;
    [SerializeField] UnityEngine.UI.Image fruitImage;
    [SerializeField] UnityEngine.UI.Button colect;

    public void UpdateSeed(ItemsData data)
    {
        seedImage.sprite = data.image;
        colect.interactable = false;
    }
    public void UpdateFruit(ItemsData data)
    {
        fruitImage.sprite = data.image;
        colect.interactable = true;
        seedImage.sprite = null;
    }

    public void Colected()
    {
        fruitImage.sprite = null;
        colect.interactable = false;
    }
}
