﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;

public class ItemShopUI : MonoBehaviour
{
    [SerializeField] ItemsData myData;
    [SerializeField] Image myImage;
    [SerializeField] Text myCost;
    [SerializeField] Text myAmount;
    [SerializeField] int myId;
    [SerializeField] int shopCost;
    [SerializeField] GameEvent buyEvent;


    public ItemsData MyData { get { return myData; } set { myData = value; } }


    public void SetItem()
    {
        shopCost = 0;
        myImage.sprite = myData.image;
        myCost.text = shopCost.ToString();
        myAmount.text = myData.amountOfItems.ToString();
        myId = myData.id;
    }

    public void AddItemToBuy()
    {
        int newAmount = myData.amountOfItems + 1;
        if (newAmount <= 10)
        {
            shopCost += myData.cost;
            myData.amountOfItems = newAmount;
            UpdateAmountItem();
            UpdateCost();
        }
    }
    public void RemoveItemToBuy()
    {
        int newAmount = myData.amountOfItems - 1;
        if (newAmount >= 0)
        {
            shopCost -= myData.cost;
            myData.amountOfItems = newAmount;
            UpdateAmountItem();
            UpdateCost();
        }
    }

    public void BuyItem()
    {
        PlayerData myPlayer = WorldController.GetPlayer();
        if (myData.amountOfItems>0&&shopCost<= myPlayer.currency&&myPlayer.myItems.Count<4)
        {
            if(!IsItemInInventory())
            {
                myPlayer.currency -= shopCost;
                myPlayer.myItems.Add(myData);
                WorldController.SavePlayer(myPlayer);
                buyEvent.Raise();
            }
      
           
        }
    }

    void UpdateAmountItem()
    {
        myAmount.text = myData.amountOfItems.ToString();
    }
    void UpdateCost()
    {
        myCost.text = shopCost.ToString();
    }

    bool IsItemInInventory()
    {
        PlayerData myPlayer = WorldController.GetPlayer();
        for (int i = 0; i < myPlayer.myItems.Count; i++)
        {
            if (myPlayer.myItems[i].id == myData.id)
            {
                myPlayer.myItems[i].amountOfItems += myData.amountOfItems;
                myPlayer.currency -= shopCost;
                WorldController.SavePlayer(myPlayer);
                buyEvent.Raise();
                return true;
            }
               
        }
        return false;
    }


}
