﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ItemInventoryUI : MonoBehaviour
{
    [SerializeField] ItemsData myData;
    [SerializeField] Image myImage;

    [SerializeField] Text myAmount;
    [SerializeField] int myId;
    [SerializeField] int shopValue;
    public ItemsData MyData { get { return myData; } set { myData = value; } }

    public void SetItem()
    {
        shopValue = myData.cost;
        myImage.sprite = myData.image;
       
        myAmount.text = myData.amountOfItems.ToString();
        myId = myData.id;
    }
 
}
