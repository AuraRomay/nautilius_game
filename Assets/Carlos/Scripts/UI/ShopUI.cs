﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ShopUI : MonoBehaviour
{
    [SerializeField] GameObject itemShopPrefab;
    [SerializeField] List<GameObject> itemsInShop;

    private void Start()
    {
        itemsInShop = new List<GameObject>();
    }

    public void AddItemToTheShop(ItemsData itemData)
    {
        GameObject tempItem = Instantiate(itemShopPrefab, transform);
        tempItem.GetComponent<ItemShopUI>().MyData = itemData;
        tempItem.GetComponent<ItemShopUI>().SetItem();
    }

   
}
