﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    
    [SerializeField] GameObject inventoryPrefab;
    [SerializeField] List<GameObject> myItems;
    [SerializeField] GameEvent buyEvent;
    [SerializeField] UnityEngine.UI.Text money;
  
    public PlayerData data;
    
    // Start is called before the first frame update
    void Start()
    {
        //WorldController.DeleatePlayer();
        buyEvent.AddEvent(UpdateItems);
        data = WorldController.GetPlayer();
        CreateInventory();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            WorldController.DeleatePlayer();
        }
       
    }
    void CreateInventory()
    {
        data = WorldController.GetPlayer();
        if(data!=null)
        {
            money.text = data.currency.ToString();
            for (int i = 0; i < data.myItems.Count; i++)
            {

                GameObject item = Instantiate(inventoryPrefab, transform);
                item.SetActive(true);
                item.GetComponent<ItemInventoryUI>().MyData = data.myItems[i];
                item.GetComponent<ItemInventoryUI>().SetItem();
                item.GetComponentInChildren<Drag>().sell = false;
                myItems.Add(item);

            }
        }
       
       
    }

    void ClearInventory()
    {
        for (int i = 0; i < data.myItems.Count; i++)
        {
            Destroy(myItems[i]);
        }
        myItems.Clear();
    }

    public void UpdateItems()
    {
        ClearInventory();
        CreateInventory();
    }
    public void OnSale()
    {
        for (int i = 0; i < myItems.Count; i++)
        {
            myItems[i].GetComponentInChildren<Drag>().sell = true;
        }
    }
    public void OnBuy()
    {
        for (int i = 0; i < myItems.Count; i++)
        {
            myItems[i].GetComponentInChildren<Drag>().sell = false;
        }
    }
    
}
