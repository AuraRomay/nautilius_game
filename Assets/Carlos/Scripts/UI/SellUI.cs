﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SellUI : MonoBehaviour
{
    [SerializeField] UnityEngine.UI.Text name;
    [SerializeField] UnityEngine.UI.Image image;
    [SerializeField] UnityEngine.UI.Text numberOfItems;
    [SerializeField] UnityEngine.UI.Text cost;


    public void SetQuantity(int quantity)
    {
        numberOfItems.text = quantity.ToString();
    }
    public void SetValue(int value)
    {
        cost.text = value.ToString();
    }
    public void SetItems(ItemsData data)
    {
        name.text = data.name;
        image.sprite = data.image;
        numberOfItems.text = "1";
        cost.text = data.value.ToString();
        Debug.Log("aaaaaaaaa");
    }
    public void ClearItem()
    {
        name.text = "";
        image.sprite = null;
        numberOfItems.text = "0";
        cost.text = "0";
    }
}
