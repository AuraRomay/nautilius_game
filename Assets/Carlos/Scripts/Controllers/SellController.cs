﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SellController : MonoBehaviour
{
   
   
    [SerializeField] int quantity;
    [SerializeField] int countCost;
    [SerializeField] ItemsData data;
    [SerializeField] SellUI myInventory;
    [SerializeField] GameEvent inevntoryEvent;
    [SerializeField] InventoryUI inventory;

    private bool CanSell=false;

    public void AddItem()
    {
        if(CanSell && quantity < data.amountOfItems)
        {

            quantity++;
            countCost += data.value;
           
            myInventory.SetQuantity(quantity);
            myInventory.SetValue(countCost);
        }
    }

    public void RemoveItem()
    {
        if (CanSell && quantity >1)
        {
            quantity--;
            countCost -= data.value;

            myInventory.SetQuantity(quantity);
            myInventory.SetValue(countCost);
        }
        else if(quantity <= 1)
        {
            quantity--;
            countCost -= data.value;
            myInventory.ClearItem();
            CanSell = false;
        }
    }

    public void Sell()
    {
        PlayerData player = WorldController.GetPlayer();
        if(CanSell)
        {
            if(!WorldController.RemoveItemOfInventory(data, quantity))
            {
                Debug.LogError("Algo salio mal con la venta");
            }
            else
            {
                int money = player.currency;
                money += countCost;
                WorldController.SetPlayeMoney(money);
                inevntoryEvent.Raise();
                myInventory.ClearItem();
                inventory.OnSale();
                quantity=0;
                countCost=0;

                myInventory.SetQuantity(quantity);
                myInventory.SetValue(countCost);
                CanSell = false;
            }
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        data = collision.GetComponentInParent<ItemInventoryUI>().MyData;
        
        SetItem();
      
        CanSell = true;
        AddItem();
    }

    void SetItem()
    {
        myInventory.SetItems(data);
    }

}
