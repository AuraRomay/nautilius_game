﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvernaderoController : MonoBehaviour
{
    [SerializeField] InvernaderosData invernaderosData;
    [SerializeField] InventoryUI myInventory;
    [SerializeField] ItemsData data;
    [SerializeField] InvernaderoUI invernaderoUI;
    [SerializeField] InvernaderosOragization invernaderosWorld;

    private bool haveSeed;

    private int invernaderoIndex;

    // Start is called before the first frame update
    void Start()
    {
        myInventory.OnSale();
    }

    // Update is called once per frame
    void Update()
    {
        if (invernaderosData.TimerEnd(Time.deltaTime))
        {
            invernaderoUI.UpdateFruit(invernaderosData.pack.fruit);

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        data = collision.GetComponentInParent<ItemInventoryUI>().MyData;
        if (invernaderosWorld.CheckSeed(data, out invernaderoIndex) && !haveSeed)
        {
            if (!WorldController.RemoveItemOfInventory(data, data.amountOfItems))
            {
                Debug.LogError("No pude eliminar el elemento");
            }
            else
            {
                myInventory.UpdateItems();
                invernaderosData.pack = invernaderosWorld.GetPack(invernaderoIndex);
                invernaderosData.pack.seed.amountOfItems = data.amountOfItems;
                invernaderoUI.UpdateSeed(invernaderosData.pack.seed);
                invernaderosData.timeStart = true;
                invernaderosData.Init();
                haveSeed = true;
            }
        }
    }
    public void Collect()
    {
        PlayerData player = WorldController.GetPlayer();
        ItemsData fruit = invernaderosData.pack.fruit;
        invernaderosWorld.ResetItems(invernaderoIndex);
        fruit.amountOfItems = data.amountOfItems;
        player.myItems.Add(fruit);
        WorldController.SavePlayer(player);
        myInventory.UpdateItems();
        myInventory.OnSale();
        haveSeed = false;
        invernaderoUI.Colected();

    }
}
