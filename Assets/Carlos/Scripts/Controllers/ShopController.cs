﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopController : MonoBehaviour
{
    [SerializeField] ElementsOfItems itemsShop;
    [SerializeField] ShopUI shop;
    // Start is called before the first frame update
    void Start()
    {
        itemsShop.ResetAmount();
        CreateShop();
       
    }

    void CreateShop()
    {
        for (int i = 0; i < itemsShop.items.Count; i++)
        {
            shop.AddItemToTheShop(itemsShop.items[i]);
        }
    }


}
