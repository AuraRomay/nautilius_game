﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class WorldController 
{
    private static string keyWorld = "Test";


   public static void CreateWorld()
    {
        if (!PlayerPrefs.HasKey(keyWorld))
        {
            WorldData newWorld = new WorldData();
            newWorld.player = new PlayerData();
            newWorld.player.myItems = new List<ItemsData>();
            string json = JsonUtility.ToJson(newWorld);
            PlayerPrefs.SetString(keyWorld, json);
            PlayerPrefs.Save();
        }
    }

    public static void SavePlayer(PlayerData player)
    {
        if (PlayerPrefs.HasKey(keyWorld))
        {
            string json = PlayerPrefs.GetString(keyWorld);
            WorldData currentWorl = JsonUtility.FromJson<WorldData>(json);
            currentWorl.player = player;
            json = JsonUtility.ToJson(currentWorl);
            PlayerPrefs.SetString(keyWorld, json);
            PlayerPrefs.Save();
        }
    }

    public static bool RemoveItemOfInventory(ItemsData item,int quantity)
    {
        if (PlayerPrefs.HasKey(keyWorld))
        {
            string json = PlayerPrefs.GetString(keyWorld);
            WorldData currentWorl = JsonUtility.FromJson<WorldData>(json);
            PlayerData player = currentWorl.player;
            for (int i = 0; i < player.myItems.Count; i++)
            {
                ItemsData playerItem = player.myItems[i];
                if (playerItem.id==item.id)
                {
                    if (playerItem.amountOfItems == quantity)
                    {
                        player.myItems.RemoveAt(i);
                        currentWorl.player = player;
                        json = JsonUtility.ToJson(currentWorl);
                        PlayerPrefs.SetString(keyWorld, json);
                        PlayerPrefs.Save();
                        return true;
                    }
                    else
                    {
                        playerItem.amountOfItems -= quantity;
                        currentWorl.player = player;
                        json = JsonUtility.ToJson(currentWorl);
                        PlayerPrefs.SetString(keyWorld, json);
                        PlayerPrefs.Save();
                        return true;
                    }
                       
                }
            }
            return false;
           
        }
        return false;
       
    }

    public static List<ItemsData> GetItems()
    {
        if (PlayerPrefs.HasKey(keyWorld))
        {
            string json = PlayerPrefs.GetString(keyWorld);
            WorldData currentWorl = JsonUtility.FromJson<WorldData>(json);
           return currentWorl.player.myItems;            
        }
        return null;
    }
    public static void SetPlayeMoney(int money)
    {
        if (PlayerPrefs.HasKey(keyWorld))
        {
            PlayerData player = new PlayerData();
            string json = PlayerPrefs.GetString(keyWorld);
            WorldData currentWorl = JsonUtility.FromJson<WorldData>(json);
            player= currentWorl.player ;
            player.currency = money;
            currentWorl.player = player;
            json = JsonUtility.ToJson(currentWorl);
            PlayerPrefs.SetString(keyWorld, json);
            PlayerPrefs.Save();
        }
    }
    public static PlayerData GetPlayer()
    {
        if (PlayerPrefs.HasKey(keyWorld))
        {
            string json = PlayerPrefs.GetString(keyWorld);
            WorldData currentWorl = JsonUtility.FromJson<WorldData>(json);
            if (currentWorl.player.myItems == null)
                currentWorl.player.myItems = new List<ItemsData>();
            return currentWorl.player;
        }
        return null;
    }
    public static void DeleatePlayer()
    {
        if (PlayerPrefs.HasKey(keyWorld))
        {
            PlayerPrefs.DeleteKey(keyWorld);
        }
    }
}
