﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFloor : MonoBehaviour
{

    public Material[] sprites;
    private int spriteValue;

    // Start is called before the first frame update
    void Start()
    {
        SpriteCreation();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpriteCreation()
    {
        spriteValue = Random.Range(0, sprites.Length);
        this.GetComponent<MeshRenderer>().material = sprites[spriteValue];
    }
}
