﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;

    private Rigidbody2D rb;
    private Vector2 moveVelocity;

    public bool interactable;
    private Invernadero scoope;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        scoope = null;
    }

    // Update is called once per frame
    void Update()
    {
        inputPlayer();
        movePlayer();
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + moveVelocity * Time.fixedDeltaTime);
    }

    void inputPlayer()
    {
        if(Input.GetMouseButtonDown(0) && interactable)
        {
            scoope.interactObject();
        }
    }

    void movePlayer()
    {
        Vector2 moveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        moveVelocity = moveInput.normalized * speed;
    }

    /*private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Interactable")
        {
            Debug.Log("Entra, interaccion");
        }
    }*/

    private void OnTriggerEnter2D(Collider2D collision)
    {
        /*if (collision.gameObject.tag == "Interactable")
        {
            Debug.Log("Entra, interaccion");
        }*/


        if (collision.GetComponent<Invernadero>() != null)
        {
            //collision.GetComponent<Invernadero>().interactObject();
            scoope = collision.GetComponent<Invernadero>();
            interactable = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<Invernadero>() == null)
        {
        interactable = false;
        scoope = null;
      
        }
    }
}
